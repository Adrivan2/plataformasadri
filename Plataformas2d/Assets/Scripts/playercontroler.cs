using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroler : MonoBehaviour
{
    public float velocidadMaxima = 5f;
    public float velocidad = 10f;
    public float saltito = 10f;
    public bool isGrounded;
    private bool teclapulsada;
    private SpriteRenderer mySpriteRenderer;
    private Rigidbody2D rb2d;
    private Animator anim;
    private bool cayendo;  
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        anim.SetBool("suelo", isGrounded);
        anim.SetBool("teclas", teclapulsada);
        anim.SetFloat("velocidad", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("salto", Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("Caida", cayendo);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.05f, LayerMask.GetMask("floor"));
        if ((hit.collider != null) && (hit.collider.CompareTag("floor")))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        if (Input.anyKey)
        {
            teclapulsada = true;
        }
        else
        {
            teclapulsada = false;

        }
        float h = Input.GetAxis("Horizontal");
        if ((h == 0) && (isGrounded == true) && (teclapulsada == false))
        {
           rb2d.velocity = Vector2.zero;
        }
        else
        {
            rb2d.AddForce(Vector2.right * velocidad * h);

            float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -velocidadMaxima, velocidadMaxima);
            rb2d.velocity = new Vector2(limitedSpeed, rb2d.velocity.y);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isGrounded==true)
            {               
                salto();               
            }            
        }  
        if (rb2d.velocity.x > 0)
        {
            mySpriteRenderer.flipX = false;
        }
        if (rb2d.velocity.x < 0)
        {
            mySpriteRenderer.flipX = true;
        }
        if (rb2d.velocity.y < 0)
        {
            cayendo = true;
        }
        if (rb2d.velocity.y > 0)
        {
            cayendo = false;
        }
    }
    private void salto()
    {       
        rb2d.velocity = new Vector2(rb2d.velocity.x, saltito);
        isGrounded = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("enemigoo"))
        {
            
            GameManager.muerto = true;
        }
    }
}

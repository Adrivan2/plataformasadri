﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Clase para gestionar el funcionamiento del juego (victoria/derrota)
/// </summary>
public class GameManager : MonoBehaviour {
    /* Singleton */
    private static GameManager instance;

    [Header("Configuración del juego")]
    [Tooltip("Número de coleccionables que se deben recoger para superar el nivel")]
    public int totalCollectablesCount;

    [Header("Mensajes de la interfaz")]
    [Tooltip("Mensaje a mostrar cuando el jugador supere el nivel")]
    public string winMessage;
    [Tooltip("Mensaje a mostrar cuando el jugador pierda la partida")]
    public string loseMessage;

    [Header("Referencias a la UI")]
    [Tooltip("Referencia al componente Text de la interfaz (OPCIONAL)")]
    public Text uiText;

    // Contador de coleccionables recogidos
    private int collectables;

    #region Funciones de Unity

    private void Awake() {
        // Singleton: Inicialización de la instancia
        if (instance == null) {
            instance = this;
        } else if (!instance.Equals(this)) {
            Destroy(this);
        }
    }

    #endregion

    #region Funciones públicas y estáticas

    /// <summary>
    /// Función para notificar que la partida se ha finalizado
    /// </summary>
    public static void GameOver() {
        // Fin del juego: mensaje por consola/interfaz y recarga la escena
        if (instance.uiText != null) {
            instance.uiText.text = instance.loseMessage;
        } else {
            Debug.Log(instance.loseMessage);
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Función para notificar que el jugador ha conseguido un coleccionable
    /// </summary>
    public static void GotCollectable() {
        // Incrementa el contador de coleccionables y comprueba si el jugador ya ha conseguido todos
        ++instance.collectables;
        if (instance.collectables == instance.totalCollectablesCount) {
            if (instance.uiText != null) {
                instance.uiText.text = instance.winMessage;
            } else {
                Debug.Log(instance.winMessage);
            }
        }
    }

    #endregion

}